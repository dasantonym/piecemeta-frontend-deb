module.exports = function (grunt) {
    'use strict';
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        debian_package: {
            main: {
                options: {
                    name: "<%= pkg.name %>",
                    maintainer: {
                        name: "Anton Koch",
                        email: "anton.koch@gmail.com"
                    },
                    short_description: "Angular based web frontend for PieceMeta.com service",
                    long_description: "Angular based web frontend for PieceMeta.com service",
                    version: "<%= pkg.version %>",
                    build_number: "0",
                    dependencies: "apache2",
                    postinst: {
                        src: 'lib/scripts/postinst'
                    },
                    prerm: {
                        src: 'lib/scripts/prerm'
                    }
                },
                files: [
                    {
                        expand: true,
                        cwd: 'bower_components/piecemeta-angular-frontend/dist/',
                        src: ['**/*'],
                        dest: '/opt/piecemeta-frontend/'
                    },
                    {
                        expand: true,
                        cwd: 'lib/apache-config/',
                        src: ['*'],
                        dest: '/etc/apache2/sites-available/'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-debian-package');

    grunt.registerTask('default', ['debian_package']);

};
